FROM registry.esss.lu.se/ics-docker/miniconda:4.7.12

USER root

RUN yum install -y \
      make \
      patch  \
      tar \
      mesa-libGL \
      mesa-libGLU \
      xorg-x11-server-Xvfb \
  && yum clean all \
  && rm -rf /var/cache/yum \
  && mkdir /build \
  && chown -R conda:users /build

COPY entrypoint_source /entrypoint_source
COPY entrypoint /entrypoint

USER conda
ENV CONDA_BUILD_VERSION=3.18.9 \
    CONDA_VERIFY_VERSION=3.4.2
RUN conda install -y \
      conda-build="${CONDA_BUILD_VERSION}" \
      conda-verify="${CONDA_VERIFY_VERSION}" \
      jinja2 \
      setuptools \
      git \
      python=3.7 \
      tini \
  && /opt/conda/bin/conda build purge-all

# Keep only conda-e3-virtual channel by default
RUN conda config --system --add channels conda-e3-virtual \
  && conda config --system --remove channels conda-e3 \
  && conda config --system --remove channels ics-conda \
  && conda config --system --remove channels anaconda-main \
  && conda config --system --remove channels conda-forge \
  && conda config --show-sources \
  && conda clean -ay

WORKDIR /build

ENTRYPOINT [ "/opt/conda/bin/tini", "--", "/entrypoint" ]
